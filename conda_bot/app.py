import logging
import sentry_sdk
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from . import pipelines, recipe_mr
from .bot import GitLabBot
from . import settings


uvicorn_logger = logging.getLogger("uvicorn.error")
logger = logging.getLogger("conda_bot")
logger.setLevel(uvicorn_logger.level)


app = GitLabBot(
    "conda-bot",
    secret=settings.GL_SECRET,
    access_token=settings.GL_ACCESS_TOKEN,
    routers=[pipelines.router, recipe_mr.router],
    url=str(settings.GL_URL),
)
if settings.SENTRY_DSN:
    sentry_sdk.init(dsn=settings.SENTRY_DSN)
    app = SentryAsgiMiddleware(app)
